package com.stefanomantini.jdk7template.configuration;

public final class Constants {

    // Profiles
    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";

    // Modifiers
    public static final String INTEGRATION_TEST = "INTEGRATION_TEST";

    private Constants() {
    }


}
