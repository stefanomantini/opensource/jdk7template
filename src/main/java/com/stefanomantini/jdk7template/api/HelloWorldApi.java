package com.stefanomantini.jdk7template.api;

import com.stefanomantini.jdk7template.domain.HelloWorld;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping(value="/api/")
public class HelloWorldApi {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value="helloWorld",
                    produces = "application/json",
                    method = RequestMethod.GET)
    public HelloWorld greeting() {
        return new HelloWorld(counter.incrementAndGet(),"Hello");
    }

}
